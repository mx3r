// Librería para obtener mmap
//  caddr_t mmap(void *start, size_t length, int prot , int flags, int fd, off_t offset);
// fd -> open()
// size/length -> stat()

#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//       fd = int open(const char *camino, int flags);
// flags -> O_RDONLY

//   int fstat(int filedes, struct stat *buf);
// 	Total_Size = buf->st_size


#include <gcrypt.h>
#include <ctype.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "util.h"


int basesz;		char *basef;
int localsz;	char *localf;
int remotesz;	char *remotef;

// **********************************************************************
// 														SPLIT PLUGIN
// **********************************************************************

void *libsplit_handle;
void (*libsplit_init)();
int (*libsplit_loadbuffer)(char *buff, int size, int nfile);
int *(*libsplit_split)(int nfile,int *pblocks);

int *base_split,*local_split,*remote_split;
int base_splitsz,local_splitsz,remote_splitsz;


int loadsplitplugin(void) {
	const char *error;
	libsplit_handle = dlopen ("./plugin-split-lines.so", RTLD_LAZY);
	if (!libsplit_handle) {
		fputs (dlerror(), stderr);
		return 0;
	}

	libsplit_init = dlsym(libsplit_handle, "init");
	libsplit_loadbuffer = dlsym(libsplit_handle, "loadbuffer");
	libsplit_split = dlsym(libsplit_handle, "split");
	if ((error = dlerror()) != NULL)  {
		fputs(error, stderr);
		return 0;
	}
	return 1;
}

int unloadsplitplugin(void) {
	dlclose(libsplit_handle);
	return 1;
}

BLRintA splitBLR(char *base, char *local, char *remote)
{
	loadsplitplugin();
	libsplit_init();

	basesz=0;basef=loadfile(base, &basesz);
	localsz=0;localf=loadfile(local, &localsz);
	remotesz=0;remotef=loadfile(remote, &remotesz);

	libsplit_loadbuffer(basef,basesz,0);
	libsplit_loadbuffer(localf,localsz,1);
	libsplit_loadbuffer(remotef,remotesz,2);

	base_split=libsplit_split(0,&base_splitsz);
	local_split=libsplit_split(1,&local_splitsz);
	remote_split=libsplit_split(2,&remote_splitsz);

//	unloadsplitplugin();

	BLRintA data;
	data.base=base_split;
	data.local=local_split;
	data.remote=remote_split;

	data.basesz=base_splitsz;
	data.localsz=local_splitsz;
	data.remotesz=remote_splitsz;


	return data;
}

// **********************************************************************
// 														HASH PLUGIN
// **********************************************************************


void *libhash_handle;
void (*libhash_init)();
int (*libhash_loadbuffer)(char *buff, int size, int nfile);
unsigned int (*libhash_hash)(int nfile,int from, int size);

int *base_hash=0,*local_hash=0,*remote_hash=0;
int base_hashsz,local_hashsz,remote_hashsz;


int loadhashplugin(void) {
	const char *error;
	libhash_handle = dlopen ("./plugin-hash-lines.so", RTLD_LAZY);
	if (!libhash_handle) {
		fputs (dlerror(), stderr);
		return 0;
	}

	libhash_init = dlsym(libhash_handle, "init");
	libhash_loadbuffer = dlsym(libhash_handle, "loadbuffer");
	libhash_hash = dlsym(libhash_handle, "hash");
	if ((error = dlerror()) != NULL)  {
		fputs(error, stderr);
		return 0;
	}
	return 1;
}

int unloadhashplugin(void) {
	dlclose(libsplit_handle);
	return 1;
}

BLRintA hashBLR(void)
{
	BLRintA data;
	loadhashplugin();
	libhash_init();

	libhash_loadbuffer(basef,basesz,0);
	libhash_loadbuffer(localf,localsz,1);
	libhash_loadbuffer(remotef,remotesz,2);

	base_hashsz=base_splitsz-1;
	local_hashsz=local_splitsz-1;
	remote_hashsz=remote_splitsz-1;

	if (base_hash) free(base_hash);
	if (local_hash) free(local_hash);
	if (remote_hash) free(remote_hash);


	base_hash=malloc(base_hashsz*sizeof(int));
	local_hash=malloc(local_hashsz*sizeof(int));
	remote_hash=malloc(remote_hashsz*sizeof(int));

	int i,from, to;
	for (i=0;i<base_splitsz-1;i++)
	{
		from=base_split[i];
		to=base_split[i+1];
		if (from>=to) printf("Error split @ block %d \n",i);
		base_hash[i]=libhash_hash(0,from,to-from);
	}
	for (i=0;i<local_splitsz-1;i++)
	{
		from=local_split[i];
		to=local_split[i+1];
		if (from>=to) printf("Error split @ block %d \n",i);
		local_hash[i]=libhash_hash(1,from,to-from);
	}
	for (i=0;i<remote_splitsz-1;i++)
	{
		from=remote_split[i];
		to=remote_split[i+1];
		if (from>=to) printf("Error split @ block %d \n",i);
		remote_hash[i]=libhash_hash(2,from,to-from);
	}

	data.base=base_hash;
	data.local=local_hash;
	data.remote=remote_hash;

	data.basesz=base_hashsz;
	data.localsz=local_hashsz;
	data.remotesz=remote_hashsz;

	return data;
}



// **********************************************************************
// 														    UTILS
// **********************************************************************


char *loadfile(char *filename, int *size) {

	int fd = open(filename, O_RDONLY);

  *size=0;
	struct stat buf;

  if (fstat(fd, &buf)==-1) return NULL;

  int filesize = buf.st_size;
	char *data=mmap(0, filesize, PROT_READ , MAP_SHARED, fd, 0);
  if ((int) data==-1) return NULL;

  *size=filesize;
	return data;
}

/*
unsigned int *hash_loadfile(char *filename, int *size) {
	char *nombre=filename, linea[MAX_LINE];
	FILE *fichero;
	fichero = fopen( nombre, "r" );

	if( !fichero ) {
		printf( "Error (NO ABIERTO)\n" );
		return NULL;
	}
	char *txt;

	int lines;
	for (lines=0;fgets(linea, MAX_LINE, fichero); lines++);
	rewind(fichero);
	*size=lines;
	unsigned int *data=malloc(lines*sizeof(unsigned int*));
	int line=0;
	while (txt=fgets(linea, MAX_LINE, fichero)) {
		reducetext(txt);
		data[line++]=ihash(txt);
	}

	if( fclose(fichero)!=0 ) {
		printf( "\nError: fichero NO CERRADO\n" );
		return NULL;
	}

	return data;

}
*/
// ihash calcula un hash de 32 bits para un texto.
unsigned int ihash(char *txt) {
	// Longitud del mensaje a cifrar
	int msg_len = strlen( txt );

	//  Longitud del hash resultante - gcry_md_get_algo_dlen
	// devuelve la longitud del resumen hash para un algoritmo
	int hash_len = gcry_md_get_algo_dlen( HASH_TYPE );

	// Salida del hash SHA1 - esto serán datos binarios
	unsigned char hash[ hash_len ];

	// Calcular el resumen SHA1. Esto es una especie de función-atajo,
	// ya que la mayoría de funciones gcrypt requieren
	// la creación de un handle, etc.
	gcry_md_hash_buffer( HASH_TYPE, hash, txt, msg_len );

	//	unsigned int ihash=*((unsigned int *)hash);
	return *((unsigned int *)hash);
}


void reducetext(char * txt) {

	int n=0, nn=0;
	char newline[256];
	char lastc=0;
	char c=txt[n];
	char type=0; // Tipos de palabras o grupos:
	// a -> texto, variable.
	// 1 -> números, con, sin decimales.
	// % -> símbolos unarios, binarios.
	// 0 -> huecos y espacios

	for (n=0;n<MAX_LINE;n++) {
		c=tolower(txt[n]); // Captura del carácter en minúscula.

		// Traducción del carácter.
		switch(c)
		{
		 	// Retonos de carro y fin de fichero: salir de la función.
			case 10:
			case 13:
			case 0:
				n=MAX_LINE; continue;
			// Tabuladores y espacios: cuentan como espacio.
			case ' ':
			case '\t':
				c=' '; break;
			// Acentos.
/*
			case 'á': c='a'; break;
			case 'é': c='e'; break;
			case 'í': c='i'; break;
			case 'ó': c='o'; break;
			case 'ú': c='u'; break;
			*/
		}

		switch(type) // Cambios de tipos según algunos datos.
		{
			case 0: // Segun si estábamos en un espacio.
				if (c>='0' && c<='9') {
					type='1';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
			case '1': // Segun si estábamos en un espacio.
				if (c>='0' && c<='9') {
					type='1';
				} else if (c=='.') {
					type='1';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
			case 'a': // Segun si estábamos en un espacio.
				if (c>='0' && c<='9') {
					type='a';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
			default:
			case '%':
				if (c==' ') {
					continue;
				} else
				if (c>='0' && c<='9') {
					type='1';
				} else if (isalpha(c)) {
					type='a';
				} else type='%';
			break;
		}
		if (c==' ') type=0;
		if (!nn && c==' ') continue; // Si está tabulando al inicio, tampoco tiene efecto.
		if (c==lastc && (type=='a' || type==0)) continue; // Desperdiciar letras repetidas.

		if (type=='%' && newline[nn-1]==' ') nn--;
		newline[nn]=c;

		nn++;
		lastc=c;
	}
	newline[nn]=0;
	strcpy(txt,newline);

}


int compare2hashvectors(int *Bvector, int Bsize, int *Mvector, int Msize,
	int MinPassSize,int MaxPassSize, hashblock *blocks, int blocksize)
{
		// Bvector: vector of hashes of Base, original or unmodified file.
		// Mvector: vector of hashes of modified file.
		// Bsize and Msize: Stores the size of their arrays.
		// MaxPassSize: (default: 256) Which is the maximum block-size of algorithm

		int line_base,line_local,size;
		int maxsize=1,total=0;
		int i,k,m;
		hashblock *bloque=blocks;
		int nbloques=0;
		int lbb=0;
		int conf_pasada[]={256,128,64,32,16,8,4,2,1,0};
		int p;
		int min_bloque=0;
		for (p=0;conf_pasada[p];p++)
		{
			min_bloque=conf_pasada[p];size=line_base=line_local=0;
			if (conf_pasada[p]>MaxPassSize) continue;
			if (conf_pasada[p]<MinPassSize) continue;

			for (i=0;i<Bsize;i+=maxsize) {
				maxsize=1;
				int j;
				for (j=0;j<nbloques;j++)
				{
					if (i>=bloque[j].line1 && i<=bloque[j].line1+bloque[j].size) break;
				}
				if (j<nbloques)
				{
					i=bloque[j].line1+bloque[j].size; continue;
				}


				for (k=0;k<Msize;k++)
				{
					int j;
					for (j=0;j<nbloques;j++)
					{
						if (k>=bloque[j].line2 && k<=bloque[j].line2+bloque[j].size) break;
					}
					if (j<nbloques)
					{
						k=bloque[j].line2+bloque[j].size; continue;
					}

					if (Bvector[i]==Mvector[k])
					{
						int nz=0,nzbl=0;

						for(m=0;k+m<Msize && i+m<Bsize;m++)
						{
							for (j=0;j<nbloques;j++)
							{
								if (k+m>=bloque[j].line2 && k+m<=bloque[j].line2+bloque[j].size) break;
								if (i+m>=bloque[j].line1 && i+m<=bloque[j].line1+bloque[j].size) break;
							}
							if (j<nbloques) break;
							if (Bvector[i+m]!=Mvector[k+m])
							{
								nz++;
								if (nz>size/8) break;
								continue;
							}
							if (nz==0) size=m;
							else
							{
								nzbl++;
								if (nzbl>2)
								{
									nzbl=0;
									nz--;
								}

							}
						}

						if (size>maxsize)
						{
							maxsize=size;
							line_base=i;
							line_local=k;
						}
					}
				}

				if (maxsize>min_bloque)
				{
					if (nbloques<blocksize)
					{
					bloque[nbloques].line1=line_base;
					bloque[nbloques].line2=line_local;
					bloque[nbloques].size=maxsize;
					nbloques++;
					} else printf( "Error: OUT OF BLOCKS. \n");


					lbb=line_base+maxsize;
					total+=maxsize;
				}

			}

		}

		int j;
		{
			hashblock auxbloque[blocksize];
			int minline=0, min_j=0;
			for (p=0;p<nbloques;p++)
			{
				minline=Bsize;
				for (j=0;j<64 && j<nbloques;j++)
				{
					if (bloque[j].line1<minline)
					{
						minline=bloque[j].line1;
						min_j=j;
					}
				}
				auxbloque[p]=bloque[min_j];
				bloque[min_j].line1=Bsize;
			}
			memcpy(bloque,auxbloque,blocksize*sizeof(hashblock));
		}
		return nbloques;
}

int is_similar(unsigned char a, unsigned char b)
{
	if (a<32) a=32;
//	else if (a<64) a=63;
//	else if (a>=128) a=127;

	if (b<32) b=32;
//	else if (b<64) b=63;
//	else if (b>=128) b=127;


	return a==b;
}

int compare2bytevectors(char *Bvector, int Bsize, char *Mvector, int Msize,
	int MinPassSize,int MaxPassSize, hashblock *blocks, int blocksize)
{
		// Bvector: vector of hashes of Base, original or unmodified file.
		// Mvector: vector of hashes of modified file.
		// Bsize and Msize: Stores the size of their arrays.
		// MaxPassSize: (default: 256) Which is the maximum block-size of algorithm
		int line_base,line_local,size;
		int maxsize=1,total=0;
		int i,k,m;
		hashblock *bloque=blocks;
		int nbloques=0;
		int lbb=0;
		int conf_pasada[]={4096,2048,1024,512,256,128,64,32,16,8,4,2,1,0};
		int p;
		int min_bloque=0;
		for (p=0;conf_pasada[p];p++)
		{
			min_bloque=conf_pasada[p];size=line_base=line_local=0;
			if (conf_pasada[p]>MaxPassSize) continue;
			if (conf_pasada[p]<MinPassSize) continue;

			for (i=0;i<Bsize;i+=maxsize) {
				maxsize=1;
				int j;
				for (j=0;j<nbloques;j++)
				{
					if (i>=bloque[j].line1 && i<=bloque[j].line1+bloque[j].size) break;
				}
				if (j<nbloques)
				{
					i=bloque[j].line1+bloque[j].size; continue;
				}


				for (k=0;k<Msize;k++)
				{
					int j;
					for (j=0;j<nbloques;j++)
					{
						if (k>=bloque[j].line2 && k<=bloque[j].line2+bloque[j].size) break;
					}
					if (j<nbloques)
					{
						k=bloque[j].line2+bloque[j].size; continue;
					}

					if (is_similar(Bvector[i],Mvector[k]))
					{
						int nz=0,nzbl=0;

						for(m=0;k+m<Msize && i+m<Bsize;m++)
						{
							for (j=0;j<nbloques;j++)
							{
								if (k+m>=bloque[j].line2 && k+m<=bloque[j].line2+bloque[j].size) break;
								if (i+m>=bloque[j].line1 && i+m<=bloque[j].line1+bloque[j].size) break;
							}
							if (j<nbloques) break;
							if (!is_similar(Bvector[i+m],Mvector[k+m]))
							{
								break;
/*								nz++;
								if (nz>size/4) break;
								continue;
*/
							}
							if (nz==0) size=m;
							else
							{
								nzbl++;
								if (nzbl>2)
								{
									nzbl=0;
									nz--;
								}

							}
						}

						if (size>maxsize)
						{
							maxsize=size;
							line_base=i;
							line_local=k;
						}
					}
				}

				if (maxsize>min_bloque)
				{
					if (nbloques<blocksize)
					{
					bloque[nbloques].line1=line_base;
					bloque[nbloques].line2=line_local;
					bloque[nbloques].size=maxsize;
					nbloques++;
					} else printf( "Error: OUT OF BLOCKS. \n");


					lbb=line_base+maxsize;
					total+=maxsize;
				}

			}

		}

		int j;
		{
			hashblock auxbloque[blocksize];
			int minline=0, min_j=0;
			for (p=0;p<nbloques;p++)
			{
				minline=Bsize;
				for (j=0;j<64 && j<nbloques;j++)
				{
					if (bloque[j].line1<minline)
					{
						minline=bloque[j].line1;
						min_j=j;
					}
				}
				auxbloque[p]=bloque[min_j];
				bloque[min_j].line1=Bsize;
			}
			memcpy(bloque,auxbloque,blocksize*sizeof(hashblock));
		}
		return nbloques;
}
