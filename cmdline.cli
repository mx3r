## Interface Description File for CLIG - command line interface generator
## author: Harald Kirsch (kirschh@lionbioscience.com)
## Adapted for autoproject by Jim Van Zandt <jrv@vanzandt.mv.com>

## The Name specified is only used in/for the manual page
Name mx3r

## Usage specifies a one-liner which is printed as part of
## usage-message. It is also used in the NAME-section of the manual
## page as a short description.
Usage {merges files looking for similar patterns, and allows several plugins
for understanding the syntax of several files }

## The Version specified is printed as part of the usage-message.
Version {0.2.0}

## Commandline specifies the name of a slot in the generated struct
## which will be set to a newly allocated string holding the
## concatenated command line. This is particularly useful for programs
## which want to record their calling sequence somewhere, e.g. in a in
## output file to document how it was generated.
Commandline tool

## These options were selected when autoproject was run.
## More options can be added at any time (see clig(1) and examples below).
String --blr do_blrmerge {<base-filename> <local-filename> <remote-filename>
Try a full (base->local to remote) merge} {count = 3,3}
String -o output_file {<output_file>
Store the mx3d result onto the specified file} {count = 1,1}
String --hashcmp do_hashcmp {<line1> <line2> ... <lineN>
Do a hash compare between 4 or more lines.} {count = 4,oo}
Flag -v be_verbose {be verbose, output debug information}
Flag --test-plugin test_plugin {try to load the split-lines plugin}
Flag --help show_help {show usage information}

########################################################################
## EXAMPLE OF FLAG OPTION

## Flag options are rather trivial. They do not have any parameters.
#Flag -v verbose {switch on verbose program operation}

########################################################################
## EXAMPLES OF STRING OPTIONS

## String options can have one or more parameters. By default they
## have exactly one parameter.
#String -title title {title of x/y-plot}

## To let them have exactly two parameters, use the following
#String -xytitles xytitles {title of x- and y-axis} \
#    {count = 2,2}

## To impose no upper limit on the number of parameters, use oo,
## i.e. double-`o'
#String -plotnames plotnames {names of curves to plot} \
#    {count = 1,oo}

## An option you really need should be made mandatory. (I'm not sure
## whether it can be called an `option' then?)
#String -colors colors {colors to use in plots} \
#    mandatory \
#    {count = 1,5}

## Non-mandatory options can have default values
#String -bg background {background color} \
#    {default = red}

########################################################################
## EXAMPLES OF FLOAT OPTIONS

## The simplest Float-option has a default-count of 1, is not
## mandatory, imposes no limit on the parameter and has no default
#Float -o offset {offset to add to all curves to plot}

## Float-option parameters can be forced to lie in a given range
#Float -p p {probability}  {range = 0,1}

## `count', `mandatory' and `default' work as for String-options
#Float -f f {frequencies} \
#    {count = 2, 10} \
#    {range = 0, 47.11 } \
#    {default = 2 4 8 16.11}

## special values for range-specs are -oo and oo denoting minus
## infinity and infinity
#Float -negscale negscale {negative scale value}     {range = -oo, 0.0}

#Float -scale scale {scale value}     {range = 0.0, oo}

########################################################################
## EXAMPLES OF INT OPTIONS

## Int-options work like Float options.
#Int -a a {flog quarx flirim poli gam i nabgala} \
#    mandatory
#
#Int -b b {ram dibum gabalabarum deri pum pam} \
#    {count = 3,4} \
#    {range = -10, 10} \
#    {default = -1 0 1}

########################################################################
## EXAMPLES OF REST COMMAND

## The Rest-command specifies what may be found on the command line
## after all options have extracted their parameters. The Rest-command
## has a default `count' of 1,oo, but here we set it to 1,10.
#Rest infiles {list of input files} \
#    {count = 1,10}


