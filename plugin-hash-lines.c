#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gcrypt.h>
#include <ctype.h>

#define HASH_TYPE GCRY_MD_SHA1

#define MAX_LINE 255

#define BASE 0
#define LOCAL 1
#define REMOTE 2

char *buffer[3];
int basefsize[3];

void init()
{
	int i;
	for (i=0;i<3;i++)
	{
		buffer[i]=NULL;
		basefsize[i]=0;
	}
}

int loadbuffer(char *buff, int size, int nfile)
{
	if (nfile<0 || nfile>2) return 0;
	buffer[nfile]=buff;
	basefsize[nfile]=size;
	return 1;
}

unsigned int hash(int nfile,int from, int size)
{
	if (size<=0) return 0;
	char *txt=malloc(size);
	if (!txt) {
		fprintf(stderr,"hash(nfile:%d,from:%d,size:%d) > malloc failed! ",nfile,from, size);
		return 0;
	}
//	memcpy(txt,&buffer[nfile][from],size);

	int i;
	char ch;
	int txt_size=0;
	char port=0;
	for (i=0;i<size;i++)
	{
		ch=buffer[nfile][from+i];
		if (ch>='a' && ch<='z') ch-='a'-'A';

		if (ch=='&') port=ch;
		if (ch>='A' && ch<='Z' && !port)
		{
			txt[txt_size]=ch;
			txt_size++;
		}
		if (port=='&' && ch==';') port=0;
	}

	//  Longitud del hash resultante - gcry_md_get_algo_dlen
	// devuelve la longitud del resumen hash para un algoritmo
	int hash_len = gcry_md_get_algo_dlen( HASH_TYPE );

	// Salida del hash SHA1 - esto serán datos binarios
	unsigned char hash[ hash_len ];

	// Calcular el resumen SHA1. Esto es una especie de función-atajo,
	// ya que la mayoría de funciones gcrypt requieren
	// la creación de un handle, etc.
	gcry_md_hash_buffer( HASH_TYPE, hash, txt, txt_size );

	//	unsigned int ihash=*((unsigned int *)hash);
	return *((unsigned int *)hash);
}
