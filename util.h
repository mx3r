#ifndef __utilh__
#define __utilh__
// Tipos de hashes :
// GCRY_MD_CRC32 - CRC32, sencillo, rápido.
// GCRY_MD_SHA1 - SHA1 potente y seguro.
#define HASH_TYPE GCRY_MD_SHA1

// Establece cual es el ancho máximo de línea para trabajar.
// Establecer como mínimo a 90. Recomendado 128 o 256.
#define MAX_LINE 1024
#define MAX_LOADED_LINES 8096

typedef struct structhashblock {
	int line1;
	int line2;
	int size;
} hashblock;

typedef struct struct_BLRintA {
	int *base,*local,*remote;
	int basesz,localsz,remotesz;
} BLRintA;

void reducetext(char * txt);
unsigned int ihash(char *txt);
unsigned int *hash_loadfile(char *filename, int *size);
char *loadfile(char *filename, int *size);

int compare2hashvectors(int *Bvector, int Bsize, int *Mvector, int Msize,
	int MinPassSize, int MaxPassSize, hashblock *blocks, int blocksize);

int compare2bytevectors(char *Bvector, int Bsize, char *Mvector, int Msize,
	int MinPassSize,int MaxPassSize, hashblock *blocks, int blocksize);

int loadsplitplugin(void);
BLRintA splitBLR(char *base, char *local, char *remote);
BLRintA hashBLR(void);

#endif

