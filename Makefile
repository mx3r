# Variables:
LIBS= -ldl -lstdc++ -lgcrypt -lgpg-error
CC=gcc
SRCS=cmdline.c mx3r.c util.c
OBJECTS=$(SRCS:.c=.o)
CFLAGS=-O9 -rdynamic -Wall

# Targets:
all: mx3r.1 mx3r plugin-split-lines.so plugin-hash-lines.so

# Linker target:
mx3r: $(OBJECTS) mx3r.1
	$(CC) $(CFLAGS) -o $@ $(OBJECTS) $(LIBS)

# Plugins
plugin-split-lines.so: plugin-split-lines.o
	ld -shared plugin-split-lines.o -o plugin-split-lines.so

plugin-hash-lines.so: plugin-hash-lines.o
	ld -shared plugin-hash-lines.o -o plugin-hash-lines.so

# Compilers:
mx3r.o: mx3r.c cmdline.o
	$(CC) $(CFLAGS) -c -o $@ $<

cmdline.o: cmdline.c mx3r.1
	$(CC) $(CFLAGS) -c -o $@ $<

util.o: util.c
	$(CC) $(CFLAGS) -c -o $@ $<

# Update manpage & cmdline.c,cmdlime.h
mx3r.1: cmdline.cli
	clig cmdline.cli

# Other targets:
run: mx3r
	./mx3r

clean:
	rm -f $(OBJECTS) mx3r plugin-split-lines.so plugin-split-lines.o plugin-hash-lines.so plugin-hash-lines.o