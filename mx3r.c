/*
	Prueba de concepto de buscador de diferencias por bloques.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// CLIG - Command Line Interpreter Generator:
#include "cmdline.h"
#include "util.h"

extern int basesz;
extern char *basef;
extern int localsz;
extern char *localf;
extern int remotesz;
extern char *remotef;

int debug=0;

void showUsage()
{
	printf("Usage: \n"); usage();
}


hashblock cb_hash[2][512];
int cb_nhash[2]={0,0};
int cb_blocks[2]={0,0};
int cb_deleted[2]={0,0};
int cb_added[2]={0,0};

void calcBytes(int nfile,int bFrom,int bTo, int lFrom, int lTo)
{
	char *baseFrom=&basef[bFrom];
	char *baseTo=&basef[bTo];
	char *localFrom=0;
	char *localTo=0;
	if (nfile==0)
	{
		localFrom=&localf[lFrom];
		localTo=&localf[lTo];
	}
	if (nfile==1)
	{
		localFrom=&remotef[lFrom];
		localTo=&remotef[lTo];
	}


	hashblock *hbchar=cb_hash[nfile]+cb_nhash[nfile];
	int totalsize=(baseTo-baseFrom)+(localTo-localFrom);

	if (debug) printf("trying %d-%d %d-%d\n",bFrom,bTo,lFrom,lTo);

	int nhbchar = compare2bytevectors(
			baseFrom, baseTo-baseFrom,
			localFrom, localTo-localFrom,
			16,totalsize/2,
			hbchar, 512-cb_nhash[nfile]);

	int totalsize1=0;
	int j;
	for (j=0;j<nhbchar;j++)
	{
		hbchar[j].line1+=bFrom;
		hbchar[j].line2+=lFrom;
		totalsize1+=hbchar[j].size;
		if (debug) printf("%d-%d %d-%d\n",hbchar[j].line1,hbchar[j].line1+hbchar[j].size,hbchar[j].line2,hbchar[j].line2+hbchar[j].size);
	}

/*	if (debug) printf("-%d +%d (%d)*\n",
		baseTo-baseFrom-totalsize1,
		localTo-localFrom-totalsize1,nhbchar);*/

	cb_nhash[nfile]+=nhbchar;

	// Estadísticas:
	cb_blocks[nfile]+=nhbchar;
	cb_deleted[nfile]+=baseTo-baseFrom-totalsize1;
	cb_added[nfile]+=localTo-localFrom-totalsize1;

}


int doBLRMerge(FILE *out, char *base, char *local, char *remote)
{
		BLRintA split=splitBLR(base, local, remote);

		if (debug) fprintf( stderr, "Base File: %d blocks found\n",split.basesz);
		if (debug) fprintf( stderr, "Local File: %d blocks found\n",split.localsz);
		if (debug) fprintf( stderr, "Remote File: %d blocks found\n",split.remotesz);

		BLRintA hash=hashBLR();

		hashblock bloque_BR[64];
		int nbloquesBR = compare2hashvectors(
												hash.base,		hash.basesz,
												hash.remote,	hash.remotesz,
												4,  					256,
												bloque_BR,		64);

		hashblock bloque_BL[64];
		int nbloquesBL = compare2hashvectors(
												hash.base,		hash.basesz,
												hash.local,	hash.localsz,
												4,  					256,
												bloque_BL,		64);

		int i;
		for (i=0;i<nbloquesBL+1;i++)
		{
			if (i==nbloquesBL)
			{
				bloque_BL[i].line1=split.basesz;
				bloque_BL[i].line2=split.localsz;
				bloque_BL[i].size=0;
			}
			if (i>0)
			{
				int b1Start=bloque_BL[i-1].line1+bloque_BL[i-1].size;
				int b2Start=bloque_BL[i-1].line2+bloque_BL[i-1].size;
				int offset1=bloque_BL[i].line1-b1Start;
				int offset2=bloque_BL[i].line2-b2Start;

				if (debug) printf("Orphan:");
				calcBytes(0,
					split.base[b1Start],
					split.base[b1Start+offset1]-1,
					split.local[b2Start],
					split.local[b2Start+offset2]-1);
			}

			if (i<nbloquesBL)
			{

				calcBytes(0,
							split.base[bloque_BL[i].line1],
							split.base[bloque_BL[i].line1+bloque_BL[i].size]-1,

							split.local[bloque_BL[i].line2],
							split.local[bloque_BL[i].line2+bloque_BL[i].size]-1
							);
			}

		}

		if (debug) printf("Total bytes deleted: %d\n", cb_deleted[0]);
		if (debug) printf("Total bytes added: %d\n", cb_added[0]);
		if (debug) printf("Total blocks used: %d\n", cb_blocks[0]);

		if (debug) printf("\n\n");


		for (i=0;i<nbloquesBR+1;i++)
		{
			if (i==nbloquesBR)
			{
				bloque_BR[i].line1=split.basesz;
				bloque_BR[i].line2=split.remotesz;
				bloque_BR[i].size=0;
			}
			if (i>0)
			{
				int b1Start=bloque_BR[i-1].line1+bloque_BR[i-1].size;
				int b2Start=bloque_BR[i-1].line2+bloque_BR[i-1].size;
				int offset1=bloque_BR[i].line1-b1Start;
				int offset2=bloque_BR[i].line2-b2Start;
				if (debug) printf("Orphan:");
				calcBytes(1,
									split.base[b1Start],
									split.base[b1Start+offset1]-1,

									split.remote[b2Start],
									split.remote[b2Start+offset2]-1
									);
			}

			if (i<nbloquesBR)
			{


				calcBytes(1,
							split.base[bloque_BR[i].line1],
							split.base[bloque_BR[i].line1+bloque_BR[i].size]-1,

							split.remote[bloque_BR[i].line2],
							split.remote[bloque_BR[i].line2+bloque_BR[i].size]-1
							);
			}

		}

		if (debug) printf("Total bytes deleted: %d\n", cb_deleted[1]);
		if (debug) printf("Total bytes added: %d\n", cb_added[1]);
		if (debug) printf("Total blocks used: %d\n", cb_blocks[1]);

		if (debug) printf("\n\n");
/*
		int j;
		for (j=0;j<cb_nhash[0];j++) // Search blocks in local for that code:
		{
			int Bfrom0=cb_hash[0][j].line1;
			int Bto0=cb_hash[0][j].line1+cb_hash[0][j].size-1;

			printf("%d->%d\n",Bfrom0,Bto0);
		}*/


		// Sort remote blocks by remote byte count:
		{
			int j,p;
			int nbloques=cb_nhash[1];
			hashblock auxbloque[nbloques];
			int minline=0, min_j=0;
			for (p=0;p<nbloques;p++)
			{
				minline=remotesz;
				for (j=0;j<nbloques;j++)
				{
					if (cb_hash[1][j].line2<minline)
					{
						minline=cb_hash[1][j].line2;
						min_j=j;
					}
				}
				auxbloque[p]=cb_hash[1][min_j];
				cb_hash[1][min_j].line2=remotesz;
			}
			memcpy(cb_hash[1],auxbloque,nbloques*sizeof(hashblock));
		}

		int fcursor=0,cursorB=0,cursorA=0;
//		fwrite(localf,sizeof(char),localsz,out);
//		fprintf(out,"\n<<<<< LOCAL <<<< \n");

		for (i=0;i<cb_nhash[1];i++)
		{
			int from=cb_hash[1][i].line2;
			int to=cb_hash[1][i].line2+cb_hash[1][i].size-1;
			if (fcursor<from)
			{
				int Bfrom=cb_hash[1][i].line1;
				if (Bfrom<cursorA+15)
				{
				// Modified data, only on remote. Copy from remote.
	//			fprintf(out,"\n<<<<< REMOTE <<<< %d bytes\n",from-fcursor);
				if (debug) fprintf(out,"|C%d>",fcursor);
				fwrite(&remotef[fcursor],sizeof(char),from-fcursor,out);
				if (debug) fprintf(out,"<C%d|",from-1);
		//		fprintf(out,"\n>>>>> REMOTE >>>>>>>>>>>\n");
				}
				else
				{

					if (debug)
					{
					fprintf(stdout,"\n<C%d<%d$",Bfrom,cursorA);
					fwrite(&remotef[fcursor],sizeof(char),from-fcursor,stdout);
					fprintf(stdout,"$C>\n");
					}
				}

				fcursor=from;
			}
			if (fcursor>=from)
			{
				// Original data: read from base, and translate to local.
//				fprintf(out,"/*");
				int Bfrom=cb_hash[1][i].line1;
				int Bto=cb_hash[1][i].line1+cb_hash[1][i].size-1;
				int j;
				//				printf("**** %d %d->%d\n",i,Bfrom,Bto);
				int prev=0;
				for (j=0;j<cb_nhash[0];j++) // Search blocks in local for that code:
				{
					int Bfrom0=cb_hash[0][j].line1;
					int Bto0=cb_hash[0][j].line1+cb_hash[0][j].size-1;

					int f=(Bfrom0>Bfrom)? Bfrom0:Bfrom;
					int t=(Bto0<Bto)? Bto0:Bto;

					if (t>f)
					{
						if (!prev && f>Bfrom)
						{
							if (debug) fprintf(out,"{Bf%d}",Bfrom);
							int From2;
							if (j>0)
							{
								From2=cb_hash[0][j-1].line2+cb_hash[0][j-1].size;
							}
							else From2=0;

							int To2=cb_hash[0][j].line2;

							if (cursorB>From2) From2=cursorB;
							if (debug) fprintf(out,"|B%d>",From2);
							fwrite(&localf[From2],sizeof(char),To2-From2,out);
							if (debug) fprintf(out,"<B%d|",To2);
							cursorB=To2;

						}
						prev=1;

						if (debug) fprintf(out,"|A%d>",f);
						fwrite(&basef[f],sizeof(char),t-f,out);
						if (debug) fprintf(out,"<A%d|",t);
						cursorA=t;

						if (t==Bto0 && t<Bto)
						{
							int From2=cb_hash[0][j].line2+cb_hash[0][j].size-1;
							int To2=cb_hash[0][j+1].line2;


							if (cursorB>From2) From2=cursorB;
							//if (To2-From2>Bto-t) To2=From2+Bto-t;
							if (debug) fprintf(out,"|b%d>",From2);
							fwrite(&localf[From2],sizeof(char),To2-From2,out);
							if (debug) fprintf(out,"<b%d|",To2);
							cursorB=To2;
						}

					} else prev=0;

				}
//				fprintf(out,"*/");
				/*
				fprintf(out,"|C2>");
				fwrite(&remotef[fcursor],sizeof(char),to-fcursor,out);
				fprintf(out,"<C2|");*/
				fcursor=to;
			}

		}

		// fwrite(ptr,sizeof(char),size,out);

		return 0;

}

int main(int argc, char **argv) {
	// Parse the command line args.
	Cmdline *cmd = parseCmdline(argc, argv);

	if (cmd->show_helpP) showUsage();

	if (cmd->be_verboseP) debug=1;

	if (cmd->do_blrmergeP)
	{
		FILE *of1;
		int result;
		if (cmd->output_fileP)
		{
			of1=fopen(cmd->output_file,"w");
		}
		else of1=stdout;

		result= doBLRMerge( of1,
				cmd->do_blrmerge[0],
				cmd->do_blrmerge[1],
				cmd->do_blrmerge[2]);

		if (cmd->output_fileP)
		{
			fclose(of1);
		}
		return result;
	}
	if (cmd->test_pluginP)
	{
		printf("\n Intentando cargar el plugin... \n");
		if (loadsplitplugin())
		{
			printf("\n - El plugin se cargó correctamente -\n");
			return 0;
		} else
		{
			printf("\n ** ERROR \n");
			return 1;
		}
	}
	if (cmd->do_hashcmpP)
	{
		int i,b;
		int nhashes=cmd->do_hashcmpC-1;
		int block_size=4;
		unsigned int *int_hash=malloc((nhashes)*sizeof(unsigned int));
		fprintf( stderr, "nhashes: %d\n",nhashes );
		for (i=1;i<cmd->do_hashcmpC;i++)
		{
			int_hash[i-1]=ihash(cmd->do_hashcmp[i]);
		}

		for (i=0;i<nhashes-block_size+1;i++)
		{
			for (b=0;b<block_size;b++)
			{
				printf( "%08X", int_hash[i+b]);
			}
			printf( "\n");
		}
		free(int_hash);
		return 0;
	}

	printf("* Nothing to do.\n");
	showUsage();
	return 1;
}
