#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 255

#define BASE 0
#define LOCAL 1
#define REMOTE 2

char *buffer[3];
int basefsize[3];

int block[3][65535];
int nblocks;

void init()
{
	int i;
	for (i=0;i<3;i++)
	{
		buffer[i]=NULL;
		basefsize[i]=0;
	}
	nblocks=0;
}

int loadbuffer(char *buff, int size, int nfile)
{
		if (nfile<0 || nfile>2) return 0;
		buffer[nfile]=buff;
		basefsize[nfile]=size;
		return 1;
}

int *split(int nfile,int *pblocks)
{
	int i, lastblock=0;
	nblocks=0;

	block[nfile][nblocks]=0; // 0 es la primera marca SIEMPRE
	// (excepto que se quiera excluir la cabecera)
	nblocks++;
	int do_block;
	for (i=0;i<basefsize[nfile];i++)
	{
		do_block=0;
		if (buffer[nfile][i]==10 || buffer[nfile][i]==13)
				do_block=1;
		if (i-lastblock>32 && buffer[nfile][i]<=32) do_block=1;
		//if (i-lastblock>4 && (buffer[nfile][i-1]=='{' || buffer[nfile][i+1]=='}')) do_block=1; // C / C++

		if (do_block)
		{
			block[nfile][nblocks]=i;
			lastblock=i;

			nblocks++;
		}
	}
	i=basefsize[nfile]-1;
	if (i>lastblock) // Agregar la marca del último bloque
	{ // (Excepto que se quiera excluir el pie)
		block[nfile][nblocks]=i;
		lastblock=i;

		nblocks++;
	}

	for (i=0;i<nblocks-1;i++)
	{
		if (block[nfile][i]>=block[nfile][i+1]) printf("Error split @ block %d, file %d \n",i,nfile);
	}


	*pblocks=nblocks;
	return block[nfile];
}
